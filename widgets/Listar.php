<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;


class Listar extends Widget{
    
    public $modelos;

    public function init(){
        parent::init();
//        if($this->message===null){
//            $this->message= 'Hola Mundo';
//        }
    }

    public function run(){
        return $this->render("_trabajadores", [
            "modelos" => $this->modelos,
        ]);
    }
}
