<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Consulta 12';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row row-flex-wrap">

<?php
    foreach ($modelos as $modelo) {
?>
    
<div class="site-index">

    <div class="col-sm-6 col-md-4 flex-grow">     <!--flex-grow    Es una clase de home.css, ajusta a la foto-->
        <div class="thumbnail">
            <div class="caption">
                <h3> <?= $modelo->id ?> </h3>
                <ul class="list-group">
                    <li class="list-group-item">Nombre: <?= $modelo->nombre ?> </li>
                    <li class="list-group-item">Apellidos: <?= $modelo->apellidos ?> </li>
                </ul>
                <figure>
                    <?= Html::img("@web/imgs/$modelo->foto", ['class' => 'img-responsive']); ?>
                </figure>
            </div>
        </div>
    </div> 

</div>
<?php
    }
?>

