--
-- Definition for database ejemplo2yii2
--
DROP DATABASE IF EXISTS ejemplo2yii2;
CREATE DATABASE IF NOT EXISTS ejemplo2yii2
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE ejemplo2yii2;

--
-- Definition for table delegacion
--
CREATE TABLE IF NOT EXISTS delegacion (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  poblacion varchar(255) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Definition for table trabajadores
--
CREATE TABLE IF NOT EXISTS trabajadores (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  apellidos varchar(255) DEFAULT NULL,
  fechaNacimiento date DEFAULT NULL,
  foto varchar(255) DEFAULT NULL,
  delegacion int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_trabajadores_delegacion_id FOREIGN KEY (delegacion)
  REFERENCES delegacion (id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

-- 
-- Dumping data for table delegacion
--
INSERT INTO delegacion VALUES
(1, 'alpe', 'santander', 'pasaje de peña 1 '),
(2, 'saz', 'torrelavega', 'saiz 1'),
(3, 'apd', 'torrelavega', 'vargas 3'),
(4, 'torrelavega', 'laredo', 'laiz 1'),
(5, 'principal', 'asturias', '3'),
(6, 'filial1', 'maliaño', '2');

-- 
-- Dumping data for table trabajadores
--
INSERT INTO trabajadores VALUES
(1, 'ramon', 'abramo ', '1972-11-10', 'ramon.jpg', 1),
(2, 'jose', 'gomez', '2000-10-10', 'josegomez.jpg', 1),
(3, 'silva', 'sanz', '1980-09-20', 'silva.jpg', 2),
(4, 'ana', 'ruiz', '1980-09-20', 'ana.jpg', 3),
(5, 'silvia', 'perez', '1990-09-20', 'silvia.jpg', 3),
(6, 'ana', 'zamora', '1982-10-20', 'ana.jpg', 4),
(7, 'chelo', 'ruiz', '1981-09-01', 'chelo.jpg', 5),
(8, 'pepe', 'plaza', '1982-09-20', 'pepe.jpg', 1),
(9, 'luis', 'pons', '1983-04-21', 'luis.jpg', 1);

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;

select * from delegacion;

select * from trabajadores;