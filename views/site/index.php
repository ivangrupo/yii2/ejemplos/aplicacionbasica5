<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */

$this->title = 'Aplicación Básica 5';
?>
<div class="site-index">

    <div class="jumbotron">
        
        <h1>Consultas de la Aplicación Básica 5</h1>
    
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 12</h2>

                <p>Con activeRecord.
                    <br> - Mostrar todos los trabajadores de la tabla trabajadores</p>

                <p><?= Html::a('Consulta 12', ['site/consulta12'], ["class" => "btn btn-lg btn-success"]) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 14</h2>

                <p>Con activeRecord, createCommand y Query builder.
                    <br> - Mostrar todas las delegaciones de la tabla delegaciones</p>
                
                <p><?= Html::a('Consulta 14', ['site/consulta14'], ["class" => "btn btn-lg btn-success"]) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 17</h2>

                <p>Con activeRecord, createCommand.
                    <br>- Listar todas las delegaciones de la tabla delegaciones con poblacion santander.
                    <br>- Listar todos los trabajadores de la tabla trabajadores de la delegacion 1.
                    <br>- Listar todos los trabajadores de la tabla trabajadores ordenados por nombre.
                    <br>- Listar todos los trabajadores de la tabla trabajadores sin fechaNacimiento.
                </p>

                <p><?= Html::a('Consulta 17', ['site/consulta17'], ["class" => "btn btn-lg btn-success"]) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 20</h2>

                <p>Con activeRecord, createCommand.
                    <br>- Listar las delegaciones con población distinta a santander que tengan dirección.
                    <br>- Listar los trabajadores y delegaciones de los trabajadores con foto.
                    <br>- Listar los trabajadores y las delegaciones de los trabajadorse con foto.
                    <br>- Sacar las delegaciones sin trabajadores.
                </p>

                <p><?= Html::a('Consulta 20', ['site/consulta20'], ["class" => "btn btn-lg btn-success"]) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 22</h2>

                <p>Con activeRecord, createCommand, arrayHelper.
                    <br>- Listar el nombre y apellidos de todos los trabajadores concatenados y denominar al campo "nombre completo".
                </p>

                <p><?= Html::a('Consulta 22', ['site/consulta22'], ["class" => "btn btn-lg btn-success"]) ?></p>
            </div>
        </div>

    </div>
</div>